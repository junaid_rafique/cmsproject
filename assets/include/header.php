<?php
    include("../model/Post_class.php");
     $post1 = new Post_class();
?>
<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
    <meta name="description" content="">
    <meta name="author" content="">



    <title>PHP OOP BLOG</title>



  </head>

  <body>

    <div class="blog-masthead">
      <div class="container">
        <nav class="blog-nav">
          <a class="blog-nav-item active" href="">Home</a>
          <a class="blog-nav-item" href="../views/add_user.php">Add User</a>
          <a class="blog-nav-item" href="../views/view_users.php">View Users</a>

        </nav>
      </div>
    </div>

    <div class="container">

      <div class="blog-header">
        <h1 class="blog-title">PHP OOP CMS</h1>
        <p class="lead blog-description">The official example template of creating a blog with Bootstrap.</p>
      </div>

      <div class="row">

        <div class="col-sm-8 blog-main">

                    <?php
                          $post_show = $post1->selectpost('post');
                          foreach($post_show as $post)
                             {
                            ?>


          <div class="blog-post">
            <h2 class="blog-post-title"><span style="color: #D5CD2D">Title: </span><?php echo $post["Title"]; ?></h2>


            <p class="blog-post-meta"> <span style="color: #D5CD2D">Auther Name: </span><i><?php echo $post["UserName"]; ?></i></p>
            <p class="blog-post-meta"><span style="color: #D5CD2D">Time: </span><u><?php echo $post["Time"]; ?></u></p>
            <div class="blog-post-meta"> <img src="<?php echo $post["Picture"]; ?>" height="250x" width="500px"></div>
            <p class="blog-post-meta"><span style="color: #D5CD2D">Description: <br></span><?php echo $post["Body"]; ?></p>  

            <a  class="btn btn-info" href="../views/edit_post.php?id=<?php echo $post["id"];?>">Edit Post</a>
            <a  id="readmore" class="btn btn-link " href="../views/comment.php?id=<?php echo $post["id"];?>">Read More</a><br>


            <hr>
            <?php
                            }
                         ?>


<!-- bubble animation snipit -->
                         <div id="background-wrap">
                             <div class="bubble x2"></div>
                             <div class="bubble x3"></div>
                             <div class="bubble x4"></div>
                             <div class="bubble x5"></div>
                         </div>

          </div><!-- /.blog-post -->
      </div>
    </div>

          <!-- Custom CSS -->
          <link rel="stylesheet" type="text/css" href="../css/template.css">
          <link rel="stylesheet" type="text/css" href="../css/bootstrap.min.css">
          <script type="text/javascript" src="../js/bootstrap_js.min.js"></script>
