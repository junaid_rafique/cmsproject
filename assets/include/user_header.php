<?php
    include("../model/User_class.php");
?>
<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
    <meta name="description" content="">
    <meta name="author" content="">
    <title>Admin Panel</title>
  </head>
<script type="text/javascript" src="../assets/js/jquery-3.3.1.min.js"></script>
  <body>
    <div class="blog-masthead">
      <div class="container">
        <nav class="blog-nav">
          <a class="blog-nav-item" href="../views/index.php">Home</a>
          <a class="blog-nav-item" href="../views/add_user.php">Add User</a>
          <a class="blog-nav-item active" href="../views/view_users.php">View Users</a>
        </nav>
      </div>
    </div>
    <div class="container">
      <div class="row">
        <div class="col-sm-12 blog-main">
            <!-- display categories-->
            <table class="table table-striped">
                           <tr align="center">
                               <td colspan="8"><h1 style="color: #20897A">All Users</h1></td>
                           </tr>
                           <tr>
                            <th>User ID</th>
                            <th>User Name</th>
                            <th>User Email</th>
                            <th>Edit User</th>
                            <th>Add New Post</th>
                            <th>Delete User</th>
                          </tr>
                          <?php
                          $post_data = $user1->selectUser('users');
                          foreach($post_data as $post)
                             {
                            ?>
                          <tr style="background: #12292E">
                            <td style="background: #05223F; color: yellow"><?php echo $post["id"]; ?></td>
                            <td style="background: #05223F; color: silver"><?php echo $post["name"]; ?></td>
                            <td style="background: #05223F; color: silver"><?php echo $post["address"]; ?></td>


                              <td><a href="edit_user.php?id=<?php echo $post["id"];?>"class="btn btn-primary">Edit</a></td>
                              <td><a href="../views/add_post.php?id=<?php echo $post["id"];?>&name=<?php echo $post["name"];?>" class="btn btn-success">Add post</a></td>
                              <td><a href="../model/User_class.php?uid=<?php echo $post["id"]; ?>" name="deletuser"class="btn btn-danger">Delete</a></td>
                          </tr>
                          <?php
                            }
                         ?>
                    </table>




                </div>
          </div>
      </div>

<style>
th{
  color: #A5EED7
}
</style>

</body>
</html>
        <!-- Custom CSS -->
