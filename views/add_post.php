<?php
    include("../model/Post_class.php");
    include "../assets/validation/postValidation.php";


      $post_title = "";
      $post_content ="";
      $post_image = "";
    

?>
<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
    <meta name="description" content="">
    <meta name="author" content="">
      <!-- Styling -->
      <link rel="stylesheet" type="text/css" href="../assets/css/bootstrap.min.css">
      <script type="text/javascript" src="../assets/js/bootstap_jq.min.js"></script>
      <script type="text/javascript" src="../assets/js/bootstrap_js.min.js"></script>
      <link rel="stylesheet" type="text/css" href="../assets/css/template.css">
      <link rel="stylesheet" href="../assets/css/style.css">
      <script type="text/javascript" src="../assets/js/img_view.js"></script>
    <title>Create a new post</title>
  </head>

  <body>
    <div class="blog-masthead">
      <div class="container">
        <nav class="blog-nav">
          <a class="blog-nav-item" href="index.php">Home</a>
          <a class="blog-nav-item" href="add_user.php">Add User</a>
          <a class="blog-nav-item  active" href="view_users.php">Add Post</a>

        </nav>
      </div>
    </div>

    <div class="container">

      <div class="row">

        <div class="col-sm-12 blog-main">
          <br>

          <h2 style="color: #F9C89F">Add New Post:</h2><hr>
          <form action="" method="post" enctype="multipart/form-data">
    <div class="form-group">
      <label style="color: #D5CD2D">Post Title:</label><span class="alert-danger"> <?php echo $title_err;?> </span>
    <input type="text" name="title" value="<?php echo $post_title; ?>" class="form-control" placeholder="Enter title">
  </div>
  <div class="form-group">
    <label style="color: #D5CD2D">Content:</label><span class="alert-danger"> <?php echo $content_err;?> </span>
    <textarea name="content" value="<?php echo $post_content;?>" class="form-control" rows="3" placeholder="Enter content..." ><?php echo $post_content;?></textarea>
    
  </div>
  <div class="form-group">
          <input  type="file" name="image" value="<?php $_FILES["image"]["name"]->img;?>" />

       </div>
  <button  name="postsubmit" class="btn btn-success">Submit</button>

</form>
        </div>
      </div>
    </div>

  </body>

</html>
