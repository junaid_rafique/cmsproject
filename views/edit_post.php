<?php
    include("../model/Post_class.php");

?>
<!DOCTYPE html>
<html>
  <head>
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">
    <title>Create a new post</title>
  </head>
  <body>
    <div class="blog-masthead">
      <div class="container">
        <nav class="blog-nav">
          <a class="blog-nav-item" href="index.php">Home</a>
          <a class="blog-nav-item active" href="edit_post.php">Edit Post</a>
          <a class="blog-nav-item  " href="view_users.php">View Users</a>
        </nav>
      </div>
    </div>
    <div class="container">
      <div class="row">
        <div class="col-sm-12 blog-main">
          <br>
          <h2>Edit Post:</h2><hr>
          <?php
          if(isset($_GET["id"]))  
         {  
            $post_id = $_GET["id"];
            $post_data = $post1->selectWhere($post_id);
           while($row = mysqli_fetch_array($post_data)){
   ?> 

        <form action="#" method="post" enctype="multipart/form-data">
  <div class="form-group">
    <label>Post Title:</label>
    <input type="text" name="title" class="form-control" value= "<?php echo $row["Title"]; ?>">
  </div>
  <div class="form-group">
    <label >Content:</label>
    <textarea name="content" class="form-control" rows="3" value= "<?php echo $row["Body"]; ?>" >
      <?php echo $row["Body"];?></textarea>
  </div>
  <div class="form-group">
          <input  type="file" name="image" value="<?php $_FILES["image"]["name"]->img;?>" />
       </div>
<br>
<?php  
}
 }  
?> 
  <button href = "#?id=<?php echo $post["id"];?>" name="updatepost" class="btn btn-success">Update</button>
  <a href="index.php" class="btn btn-danger">Cancel</a>
</form>
<!-- Styling -->
<link rel="stylesheet" type="text/css" href="../assets/css/bootstrap.min.css">
<script type="text/javascript" src="../js/assets/bootstrap_jq.min.js"></script>
<script type="text/javascript" src="../js/assets/bootstrap_js.min.js"></script>
<link rel="stylesheet" type="text/css" href="../assets/css/template.css">
<!-- Styling end -->
