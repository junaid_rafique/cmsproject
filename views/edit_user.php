<?php
    include("../model/User_class.php");
    if(isset($_GET["id"])){
       $u_id = $_GET["id"];
    }
    if(isset($_POST["update"])){
      $user_name = $_POST["name"];
      $user_email = $_POST["email"];
      $user_id = $u_id;
      if ($user_name=='' ||  $user_email=='')
    {
        echo "<META HTTP-EQUIV='Refresh' CONTENT='60; URL=add_user.php'>";      

    }
    else{
    
      $update_query = $user1->updateUser($user_id , $user_name, $user_email);
      echo "<META HTTP-EQUIV='Refresh' CONTENT='0; URL=view_users.php'>";       
      
    }
      if($update_query){
         echo "Query working";
      }
      else{
          echo "Query not working.";
      }

  }

?>
<!DOCTYPE html>
<html>
<head>
  <title></title>
</head>
<body><!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
    <meta name="description" content="">
    <meta name="author" content="">

    <title>Edit User</title>

  </head>

  <body>

    <div class="blog-masthead">
      <div class="container">
        <nav class="blog-nav">
          <a class="blog-nav-item" href="index.php">Home</a>
          <a class="blog-nav-item active" href="add_user.php">Edit User</a>
          <a class="blog-nav-item" href="view_users.php">View Users</a>
        </nav>
      </div>
    </div>
    <div class="container">
      <div class="row">
        <div class="col-sm-12 blog-main">
          <br>
          <h2 style="color: #F9C89F">Edit User:</h2><hr>
          
<?php

            $user_data = $user1->selectWhere($u_id);
           while($row = mysqli_fetch_array($user_data)){
   ?> 
  <form action="" method="post">
  <div class="form-group">
    <label style="color: #D5CD2D">User Name:</label>
    <input type="text" name="name" class="form-control" value= "<?php echo $row["name"]; ?>">
  </div>

  <div class="form-group">
    <label style="color: #D5CD2D">Email:</label>
    <input  type="text" name="email" class="form-control" value="<?php echo $row["address"];?>">

  </div>
  <?php  

 }  
?> 
 
  <button type="submit" name="update" class="btn btn-success">Update User</button>
  <a href="view_users.php" class="btn btn-danger">Cancel</a>
</form>

<!-- Styling -->
<link rel="stylesheet" type="text/css" href="../assets/css/bootstrap.min.css">
<script type="text/javascript" src="../js/assets/bootstrap_jq.min.js"></script>
<script type="text/javascript" src="../js/assets/bootstrap_js.min.js"></script>
<link rel="stylesheet" type="text/css" href="../assets/css/template.css">
<!-- Styling end -->

</body>
</html>
