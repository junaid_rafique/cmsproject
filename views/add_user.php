<?php

  include("../assets/validation/userValidation.php");
    include("../model/User_class.php");
?>
<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
    <meta name="description" content="">
    <meta name="author" content="">

    <title>Add User</title>

  </head>

  <body>

    <div class="blog-masthead">
      <div class="container">
        <nav class="blog-nav">
          <a class="blog-nav-item" href="index.php">Home</a>
          <a class="blog-nav-item active" href="add_user.php">Add User</a>
          <a class="blog-nav-item" href="view_users.php">View Users</a>
        </nav>
      </div>
    </div>

    <div class="container">

      <div class="row">

        <div class="col-sm-12 blog-main">
          <br>

          <h2 style="color: #F9C89F">Add New User:</h2><hr>
        <form action="add_user.php" method="post">

  <div class="form-group">
    <label style="color: #D5CD2D">User Name:</label><span class="alert-danger"> <?php echo $err_name;?> </span>
     <input type="text" name="name" value="" class="form-control">
  </div>

  <div class="form-group">
    <label style="color: #D5CD2D">Email:</label><span class="alert-danger"> <?php echo $err_email;?> </span>
    <input type="text" name="email" value="" class="form-control" >
  </div>

<button name="submit" class="btn btn-success" >Add User
</a>
</form>
</div>
</div>
</div>
<!-- Styling -->
<link rel="stylesheet" type="text/css" href="../assets/css/bootstrap.min.css">
      <script type="text/javascript" src="../assets/js/bootstap_jq.min.js"></script>
      <script type="text/javascript" src="../assets/js/bootstrap_js.min.js"></script>
      <link rel="stylesheet" type="text/css" href="../assets/css/template.css">
      <link rel="stylesheet" href="../assets/css/style.css">
<!-- Styling end -->
</body>


</html>
