<?php

	class post {
		Private $Post_id;
		private $User_id;
		private $Title="";
		Private $Body="";
		
		public function __construct($title, $body)
	{
			
			$this->Title =$title;
			$this->Body = $body;
		
		}
		function set_Title($new_Title) 
		{
			 $this->Title = $new_Title;
		}
	 
		function get_Title()
		 {
		 	return $this->Title;
		  }
		  function set_Body($new_Body) 
		{
			 $this->Body = $new_Body;
		}
	 
		function get_Body()
		 {
		 	return $this->Body;
		  }

	}

	$data = new Databases;
	$msg = '';
 	$userid = $_GET['id'];
	$username = $_GET['name'];

	if(isset($_POST["PostSubmit"]))
	{
	$post1 = new post();
	$filename = $_FILES["image"]["name"]; 
	$temp = $_FILES["image"]["tmp_name"]; 
	$folder= "../dbpics/images".$filename; 
	move_uploaded_file( $temp , $folder);

	 $insert_data = array(
	 'UserId' => mysqli_real_escape_string($data->con, $userid),
	 'Title' => mysqli_real_escape_string($data->con, $post1->get_Title()),
	 'Body' => mysqli_real_escape_string($data->con, $post1->get_Body()),
	 'Picture' => mysqli_real_escape_string($data->con, $folder),
	 'UserName' => mysqli_real_escape_string($data->con, $username),
	 );

	 if($data->insert('posts', $insert_data))
	 {
	 $msg = "Inserted Data Successfully!";
	 }

	}

if(isset($_POST["update"])) 

 { 
  $post1 = new post();

	$filename = $_FILES["image"]["name"]; 
	$temp = $_FILES["image"]["tmp_name"]; 
	$folder= "../dbpics/images".$filename; 
	move_uploaded_file( $temp , $folder);
      $update_data = array(  
           'Title'     =>     mysqli_real_escape_string($data->con, $post1->get_Title()),  
           'Body'          =>     mysqli_real_escape_string($data->con, $post1->get_Body()),
	 		'Picture' => mysqli_real_escape_string($data->con, $folder),
      );  
      $where_condition = array(  
           'id'     =>     $_GET["id"]  
      );  
      if($data->update("posts", $update_data, $where_condition))  
      {  
           header("location:userpost.php?updated=1");  
      }  
 }  
 if(isset($_GET["updated"]))  
 {  
      $success_message = 'Post Updated';  
 }

if(isset($_POST["delete"]))  
 {  
      $where = array(  
           'id'     =>     $_GET["post_id"]  
      ); 
echo $_GET["post_id"];
      if($data->delete("posts", $where))  
      {  
           header("location:userpost.php?deleted=1");  
      } 
    
 }  	


?>
